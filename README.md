# InformationRetrieval

## Instalación

1. Tener instalado previamente el compilador para Fortran
2. Instalar requerimientos

        pip install -r requiriments.txt

3. Instalar los complementos de NLTK:: stopwords. 

        python -c import NLTk; NLTK.download()

## Configuración

1. En `settings.py` configurar los siguientes campos:
        
        # Path del corpus en formato json (El campo sobre el que se hará la recuperación de similaridad debe estar lematizado)
        GENSIM_STEMMED_JSON_DIR = '/home/paulo/Downloads/gensim_dataa/data.json'
              
        # Path donde se guardarán los archivos generados por Gensim
        GENSIM_GENERATED_DATA_DIR = '/home/user/Downloads/gensim_dataa/'

        # Llave del corpus sobre la que se realizará la recuperación de similaridad
        SIMILARITY_KEY = 'noticia'

        # Lenguaje del lematizador, utilizado para definir las stopwords y la lematización de las consultas.
        # Lenguajes soportados: "danish dutch english finnish french german hungarian italian norwegian porter portuguese romanian russian spanish swedish"
        STEMMER_LANGUAGE = 'spanish'
        
2. Configurar la base de datos:

        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                'NAME': 'information_retrieval',
                'USER': 'root',
                'PASSWORD': '',
                'HOST': '',
                'PORT': '',
            }
        }

## Uso

1. Procesamiento del corpus, ejecutar la primera vez o si se modificó el corpus (solo en el caso de haber ampliado, reducido el corpus o haber modificado el contenido del algún campo con llave SIMILARITY_KEY del corpus):

        python manage.py initialize

2. Levantar el servidor:

        python manage.py runserver