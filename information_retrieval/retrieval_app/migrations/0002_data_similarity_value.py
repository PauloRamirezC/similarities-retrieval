# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('retrieval_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='data',
            name='similarity_value',
            field=models.TextField(null=True),
        ),
    ]
