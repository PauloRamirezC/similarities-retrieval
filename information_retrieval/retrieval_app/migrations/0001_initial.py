# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.TextField(null=True)),
                ('categoria', models.CharField(max_length=70, null=True)),
                ('url', models.TextField(null=True)),
                ('fecha', models.DateField(null=True)),
            ],
            options={
                'db_table': 'data',
            },
        ),
    ]
