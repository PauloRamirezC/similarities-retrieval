from django.shortcuts import render
from django.conf import settings
from functions.gensim_json import MyCorpus
from .models import Data
import json

# loading into memory pre-processed data (python manage.py initialize)
corpus_object = MyCorpus(settings.SIMILARITY_KEY)
corpus_object.load_dictionary()
corpus_json = json.load(open(settings.GENSIM_STEMMED_DATA_JSON_DIR))

def index(request):

    search = request.GET.get('search') or request.POST.get('search')

    if search:

        begin_date = request.POST.get('begin_date')
        end_date = request.POST.get('end_date')
        page = request.GET.get('page')
        excess = 0
        docs = []
        similar_docs = []
        ids_list = []
        similarities_value_list = []

        if page:
            try:
                page = int(page)
            except ValueError:
                page = 0
        else:
            page = 0

        pagination = 15
        init_corpus = page * pagination
        end_corpus = (page+1)*pagination

        similar_docs = corpus_object.query(search, init_corpus, end_corpus)
        docs_len = similar_docs['len'] / pagination

        if docs_len < page+pagination:
            excess = page+pagination - docs_len

        pages = list(xrange(page, page + pagination - excess))
        ids_list = [item[1] +1 for item in similar_docs['similarities']]   # db begins with id 1 and json with id 0
        similarities_value_list = [item[0] for item in similar_docs['similarities']]
        docs = Data.objects.filter(pk__in=ids_list)
        docs = dict([(doc.id, doc) for doc in docs])
        docs = [docs[id] for id in ids_list]

        if similarities_value_list:
            for doc in docs:
                doc.similarity_value = similarities_value_list[0]
                similarities_value_list.pop(0)

        return render(request, 'retrieval_app/index.html', {
            'docs': docs,
            'pages': pages,
            'search': search,
            'begin_date': begin_date,
            'end_date': end_date,
            'current_page': page
        })

    else:
        return render(request, 'retrieval_app/index.html', {})
