from django.db import models


class Data(models.Model):
    titulo = models.TextField(null=True)
    categoria = models.CharField(max_length=70, null=True)
    url = models.TextField(null=True)
    fecha = models.DateField(null=True)
    similarity_value = models.TextField(null=True)

    def __unicode__(self):
        return self.titulo

    class Meta:
        db_table = "data"
